## Description

User Management Graphql Api

## Installation

```bash
$ npm install

# create a .env file in the root directory with following keys
DB_TYPE= "mysql"
DB_PORT= ""
DB_NAME= ""
DB_USERNAME= ""
DB_PASSWORD= ""

```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
