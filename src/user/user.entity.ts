import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: String;

  @Column({ unique: true })
  email: String;

  @Column({ nullable: true })
  age?: number;

  @Column()
  password: string;
}
