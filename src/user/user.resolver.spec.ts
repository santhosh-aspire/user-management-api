import { Repository } from 'typeorm';
import { User } from './user.model';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';

describe('user resolver', () => {
  let userResolver: UserResolver;
  let userService: UserService;
  beforeEach(() => {
    const repository = new Repository<User>();
    userService = new UserService(repository);
    userResolver = new UserResolver(userService);
  });

  it('getUsers', () => {
    const result = [{ id: 1, name: 'test', email: 't@t.c', age: 34 }];
    jest
      .spyOn(userResolver, 'getUsers')
      .mockImplementation(() => Promise.resolve(result));

    expect(userResolver.getUsers()).resolves.toBe(result);
  });

  it('getUser success', () => {
    const result = { id: 1, name: 'test', email: 't@t.c', age: 34 };
    jest
      .spyOn(userResolver, 'getUser')
      .mockImplementation(() => Promise.resolve(result));

    expect(userResolver.getUser(1)).resolves.toBe(result);
  });

  it('getUser fail', async () => {
    const result = 'User Not Found!';
    jest
      .spyOn(userResolver, 'getUser')
      .mockImplementation(() => Promise.reject(result));

    expect(userResolver.getUser(15)).rejects.toBe(result);
  });

  it('addUser success', async () => {
    const result = { id: 1, name: 'test', email: 't@t.c', age: 34 };
    jest
      .spyOn(userResolver, 'addUser')
      .mockImplementation(() => Promise.resolve(result));

    expect(userResolver.addUser('test', 't@t.c', 34)).resolves.toBe(result);
  });

  it('addUser fails', async () => {
    const result = "Field 'name' doesn't have a default value";
    jest
      .spyOn(userResolver, 'addUser')
      .mockImplementation(() => Promise.reject(result));

    expect(userResolver.addUser('test', 't@t.c', 34)).rejects.toBe(result);
  });

  it('updateUser success', async () => {
    const result = { id: 1, name: 'test', email: 't@t.c', age: 34 };
    jest
      .spyOn(userResolver, 'updateUser')
      .mockImplementation(() => Promise.resolve(result));

    expect(userResolver.updateUser(result)).resolves.toBe(result);
  });

  it('updateUser fails', async () => {
    const result = 'User Not Found';
    jest
      .spyOn(userResolver, 'updateUser')
      .mockImplementation(() => Promise.reject(result));

    expect(userResolver.updateUser({ id: 15, name: 'sam' })).rejects.toBe(
      result,
    );
  });
  it('removeUser success', () => {
    const result = 'Removed User Successfully';
    jest
      .spyOn(userResolver, 'removeUser')
      .mockImplementation(() => Promise.resolve(result));

    expect(userResolver.removeUser(1)).resolves.toBe(result);
  });

  it('deleteUser fail', async () => {
    const result = 'User Not Found!';
    jest
      .spyOn(userResolver, 'getUser')
      .mockImplementation(() => Promise.reject(result));

    expect(userResolver.getUser(15)).rejects.toBe(result);
  });
});
