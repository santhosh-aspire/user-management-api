import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class User {
  @Field((type) => Int)
  id: number;

  @Field()
  email: string;

  @Field()
  name: string;

  @Field()
  password: string;

  @Field((type) => Int, { nullable: true })
  age?: number;
}

@InputType()
export class UpdateUserInput {
  @Field((type) => Int)
  id: number;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  password?: string;

  @Field((type) => Int, { nullable: true })
  age?: number;
}

@ObjectType()
export class SignInOutput {
  @Field()
  token: string;
  @Field((type) => Int)
  id: number;
}
