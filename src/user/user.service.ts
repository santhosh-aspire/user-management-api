import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UpdateUserInput } from './user.model';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private user: Repository<User>) {}

  findAll() {
    return this.user.find();
  }

  find(id: number) {
    return this.user.findOne(id).then((res) => {
      if (res) return res;
      else throw new Error('User Not Found');
    });
  }

  async update(user: UpdateUserInput) {
    const userResult = await this.user.findOne(user.id);
    if (userResult) return this.user.save(user);
    else return Promise.reject('User Not Found');
  }

  remove(id: number) {
    return this.user.delete(id).then((res) => {
      console.log(res);
      if (res.affected) return 'Removed User Successfully';
      else throw new Error('User Not Found');
    });
  }

  signin(email: string, password: string) {
    return this.user.find({ email }).then((res) => {
      console.log(res);
      const user = res[0];
      if (user.password === password) {
        return { token: jwt.sign({ id: user.id }, 'secret') };
      } else {
        throw new Error('Authentication Failed');
      }
    });
  }

  signup(name: string, email: string, password: string, age?: number) {
    return this.user.insert({ name, email, password, age }).then((res) => {
      return { ...res.generatedMaps[0], name, email, age };
    });
  }
}
