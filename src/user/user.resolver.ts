import { UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query } from '@nestjs/graphql';
import { Resolver } from '@nestjs/graphql';
import { GraphqlAuthGaurd } from 'src/auth.guard';
import { SignInOutput, UpdateUserInput, User } from './user.model';
import { UserService } from './user.service';

@Resolver((of) => User)
export class UserResolver {
  constructor(private usersService: UserService) {}

  @UseGuards(new GraphqlAuthGaurd())
  @Query((returns) => [User])
  async getUsers() {
    return this.usersService.findAll();
  }

  @Query((returns) => User)
  async getUser(@Args('id', { type: () => Int }) id: number) {
    return this.usersService.find(id);
  }

  @Mutation((returns) => User)
  async signUp(
    @Args('name') name: string,
    @Args('email') email: string,
    @Args('password') password: string,
    @Args('age', { nullable: true, type: () => Int }) age?: number,
  ) {
    return this.usersService.signup(name, email, password, age);
  }

  @Mutation((returns) => SignInOutput)
  async signIn(
    @Args('email') email: string,
    @Args('password') password: string,
  ) {
    return this.usersService.signin(email, password);
  }

  @Mutation((returns) => User)
  async updateUser(@Args('user') user: UpdateUserInput) {
    return this.usersService.update(user);
  }

  @Mutation((returns) => String)
  async removeUser(@Args('id') id: number) {
    return this.usersService.remove(id);
  }
}
