import {
  createParamDecorator,
  ExecutionContext,
  Injectable,
  CanActivate,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class GraphqlAuthGaurd implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const gtx = GqlExecutionContext.create(context).getContext();
    if (!gtx.req.headers.authorization) return false;
    gtx.userId = await this.validateToken(gtx.req.headers.authorization);
    return true;
  }

  async validateToken(token: string) {
    try {
      console.log(token.split(' ')[0]);
      if (token.split(' ')[0] !== 'Bearer')
        throw new HttpException('Invalid Bearer', HttpStatus.UNAUTHORIZED);
      const userId = await jwt.verify(token.split(' ')[1], 'secret');
      console.log(userId);
      return userId;
    } catch (error) {
      console.log(error.message);
      throw new HttpException('Invalid Token', HttpStatus.UNAUTHORIZED);
    }
  }
}
